﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Button))]
    public class MenuButton : MonoBehaviour
    {
        #region Refs
        
        [SerializeField] private string _menuSceneName = "Menu";

        private Button _menu;

        #endregion

        #region UnityGameCycle

        private void Awake()
        {
            _menu = GetComponent<Button>();
        }

        private void OnEnable()
        {
            _menu.onClick.AddListener(OnMenuClick);
        }
        
        private void OnDisable()
        {
            _menu.onClick.RemoveListener(OnMenuClick);
        }

        #endregion

        #region PrivateAPI

        private void OnMenuClick()
        {
            SceneManager.LoadScene(_menuSceneName);
        }
        
        #endregion
    }
}