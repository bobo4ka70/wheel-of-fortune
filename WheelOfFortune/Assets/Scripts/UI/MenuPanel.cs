﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class MenuPanel : MonoBehaviour
    {
        #region Refs

        [SerializeField] private Button _start;
        [SerializeField] private Button _policy;

        [SerializeField] private string _gameSceneName = "Game";
        [SerializeField] private string _policySceneName = "Policy";

        #endregion

        #region UnityGameCycle

        private void OnEnable()
        {
            _start.onClick.AddListener(StartGame);
            _policy.onClick.AddListener(ShowPolicy);
        }
        
        private void OnDisable()
        {
            _start.onClick.RemoveListener(StartGame);
            _policy.onClick.RemoveListener(ShowPolicy);
        }

        #endregion

        #region PrivateAPI

        private void StartGame()
        {
            SceneManager.LoadScene(_gameSceneName);
        }
        
        private void ShowPolicy()
        {
            SceneManager.LoadScene(_policySceneName);
        }

        #endregion
        
    }
}