﻿using Facebook.Unity;
using UnityEngine;

namespace Analytics
{
    public class FacebookImpl : MonoBehaviour
    {
        #region Refs

        private const string APP_LINK_RESULT_KEY = "app_link";

        #endregion

        #region Init

        private void Awake()
        {
            if (!FB.IsInitialized)
            {
                FB.Init(GetDeeplink);
            }
            else
            {
                FB.ActivateApp();
            }
            
            DontDestroyOnLoad(gameObject);
        }

        #endregion

        #region PublicAPI

        public string GetLink()
        {
            return PlayerPrefs.GetString(APP_LINK_RESULT_KEY, string.Empty);
        }

        #endregion

        #region PrivateAPI

        private void GetDeeplink()
        {
            FB.GetAppLink(DeepLinkCallback);
        }
        
        private void DeepLinkCallback(IAppLinkResult result)
        {
            if (result.Url != null)
            {
                var resultJson = JsonUtility.ToJson(result);
                PlayerPrefs.SetString(APP_LINK_RESULT_KEY, resultJson);
                PlayerPrefs.Save();
                
                Debug.Log("Deeplink: " + result.Url);
            }
        }

        #endregion
    }
}