﻿using System;
using System.Collections.Generic;
using AppsFlyerSDK;
using UnityEngine;

namespace Analytics
{
    public class AppsFlyerObject : MonoBehaviour , IAppsFlyerConversionData
    {
        #region Refs

        [SerializeField] private string _devKey = "chNVbNUp3MmMTuCxNtLUs";
        [SerializeField] private string _appID;
        [SerializeField] private bool _getConversionData;
        
        private const string CONVERSION_DATA_KEY = "conversionData";

        #endregion

        #region Init

        private void Awake()
        {
            AppsFlyer.initSDK(_devKey, _appID, _getConversionData ? this : null);
            AppsFlyer.startSDK();
            
            DontDestroyOnLoad(gameObject);
        }

        #endregion

        #region IAppsFlyerConversionData implementation
        
        public void onConversionDataSuccess(string conversionDataJson)
        {
            Debug.Log(AppsFlyer.CallbackStringToDictionary(conversionDataJson));
            
            SaveConversionData_ToPlayerPrefs(conversionDataJson);
        }
        
        public void onConversionDataFail(string error)
        {
            AppsFlyer.AFLog("onConversionDataFail", error);
        }

        public void onAppOpenAttribution(string attributionData)
        {
            AppsFlyer.AFLog("onAppOpenAttribution", attributionData);
        }

        public void onAppOpenAttributionFailure(string error)
        {
            AppsFlyer.AFLog("onAppOpenAttributionFailure", error);
        }

        #endregion

        #region PublicAPI

        public Dictionary<string, object> GetConversionData()
        {
            return AppsFlyer.CallbackStringToDictionary(PlayerPrefs.GetString(CONVERSION_DATA_KEY, string.Empty));
        }

        #endregion

        #region PrivateAPI

        private void SaveConversionData_ToPlayerPrefs(string conversionDataJson)
        {
            PlayerPrefs.SetString(CONVERSION_DATA_KEY, conversionDataJson);
            PlayerPrefs.Save();
        }

        #endregion
    }
}